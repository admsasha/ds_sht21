#include "ds_sht21.h"

#include <Wire.h>


#define TRIGGER_TEMP_MEASURE_NOHOLD  0xF3
#define TRIGGER_HUMD_MEASURE_NOHOLD  0xF5


ds_sht21::ds_sht21(uint8_t addr) : _addr(addr) {
    Wire.begin();
}
    
float ds_sht21::getHumidity(void){
    return (-6.0 + 125.0 / 65536.0 * (float)(readSHT21(TRIGGER_HUMD_MEASURE_NOHOLD)));
}

float ds_sht21::getTemperature(void){
    return (-46.85 + 175.72 / 65536.0 * (float)(readSHT21(TRIGGER_TEMP_MEASURE_NOHOLD)));
}

uint16_t ds_sht21::readSHT21(uint8_t command)	{
    uint16_t result;

    Wire.beginTransmission(_addr);
    Wire.write(command);
    Wire.endTransmission();
    delay(100);

    Wire.requestFrom(_addr, 3);
    while(Wire.available() < 3) {}

    // return result
    result = ((Wire.read()) << 8);
    result += Wire.read();
    result &= ~0x0003;   // clear two low bits (status bits)
    return result;
}	
