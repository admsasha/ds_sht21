#ifndef DS_SHT21_H
#define DS_SHT21_H
/*
	
	Библиотека для работы с датчиком температуры и влажности SHT21
	email: dik@Inbox.ru
*/

#include <Arduino.h>

class ds_sht21 {
	public:
	    ds_sht21(uint8_t addr=0x40);

	    float getHumidity(void);
    	    float getTemperature(void);

	private:
	    uint16_t readSHT21(uint8_t command);
	    uint8_t _addr;
};


#endif
