#include <Wire.h>
#include "ds_sht21.h"

ds_sht21 SHT21;

void setup(){
    Serial.begin(9600);
}

void loop(){
    Serial.print("Humidity(%RH): ");
    Serial.print(SHT21.getHumidity());
    Serial.print("     Temperature(C): ");
    Serial.println(SHT21.getTemperature());
  
    delay(1000);
}
